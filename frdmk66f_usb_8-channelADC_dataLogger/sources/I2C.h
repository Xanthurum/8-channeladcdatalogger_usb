/*
 * I2C.h
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

#ifndef I2C_H_
#define I2C_H_

/****************************************Standard C Included Files*************************************/
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/****************************************SDK Included Files.*******************************************/
#include "fsl_common.h"
/**************************************public attributes************************************************/



/**************************************public prototypes************************************************/

    /*
     * @brief Initialisation for an i2c peripheral on the NXP-MK66F => initialisation of I2C1.
     * @param uint_32_t baudrate			-> Baudrate i2c, ex. 100000 aka 100kHz.
     * @param IRQn_Type irqnOfTheI2Cbase	-> IRQn of the I2C peripheral, ex. I2C1_IRQn.
     * @param I2C_Type* base				-> Base of the I2C peripheral, ex. I2C1.
     * @param clock_name_t clockName		-> Clockname of the I2C peripheral, ex. I2C1_CLK_SRC.
     * @param uint8_t masterAddress7bit		-> Master address of the I2C peripheral, ex. 0x01U
     */
    void I2C_initialisation(uint32_t baudrate, IRQn_Type irqnOfTheI2Cbase, I2C_Type* base, clock_name_t clockName, uint8_t masterAddress7bit);
    /*
     * @brief getter for master transfer struct
     * @return i2c_master_transfer_t masterTransfer
     */
    i2c_master_transfer_t* I2C_GetMasterTransfer(void);
    /*
     * @brief getter for master status
     * @return status_t masterStatus
     */
    status_t I2C_GetMasterStatus(void);

    /*
     * @brief I2C master transfer function.
     * @param uint8_t* bytesPtr			-> Pointer to the bytes to transfer.
     * @param uint8_t slaveAddress		-> Slave address.
     * @param i2c_direction_t direction	-> A transfer direction, aka kI2C_Write or kI2C_Read.
     * @return status_t status			-> Status of the transfer, kStatus_Success.
     */
    status_t I2C_MasterTransfer(uint8_t* bytesPtr, uint8_t slaveAddress, i2c_direction_t direction);


#endif /* I2C_H_ */








