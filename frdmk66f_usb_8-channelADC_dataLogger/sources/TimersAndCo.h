/*
 * TimersAndCo.h
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

#ifndef TIMERSANDCO_H_
#define TIMERSANDCO_H_

/****************************************Standard C Included Files*************************************/
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/****************************************SDK Included Files.*******************************************/
#include "fsl_common.h"
/**************************************public attributes************************************************/
#define GLOBAL_VAR
/**************************************public prototypes************************************************/
/*
 * @brief Initialisation of all timers and co.
 */
void TimersAndCo_initialisation(void);
/*
 * @brief Function to get the time difference.
 * @return uint16_t µs_difference	-> The time difference in µs.
 */
uint16_t TimersAndCo_GetTimeDifference(void);
/*
 * @brief Function to get the current timer value.
 * @return uint64_t µs_time	-> The timer value in µs.
 */
uint64_t TimersAndCo_GetCurrentTimerValue(void);
/*
 * @brief Function to get the current timer value pointer.
 * @return uint8_t* timeIntervalPassedPtr
 */
volatile uint8_t* TimersAndCo_GetTimeIntervalPassedFlagPtr(void);
/*
 * @brief Function to start the 100ns delay.
 */
void TimersAndCo_Start_100ns_delay(void);
/*
 * @brief Function to get the delay_100ns_PassedFlag pointer.
 * @return uint8_t* delay_100ns_PassedFlagPtr
 */
volatile uint8_t* TimersAndCo_GetDelay_100ns_PassedFlagPtr(void);
/*
 * @brief Function to start a delay in milliseconds.
 * @param uint64_t timeInMilliseconds	-> The milliseconds timer value to set.
 */
void TimersAndCo_Start_MS_delay(uint64_t timeInMilliseconds);
/*
 * @brief Function to get the delay_MS_Flag pointer.
 * @return uint8_t* delay_MS_FlagPtr
 */
volatile uint8_t* TimersAndCo_GetDelay_MS_FlagPtr(void);
#endif /* TIMERSANDCO_H_ */
