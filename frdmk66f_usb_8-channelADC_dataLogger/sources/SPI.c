/*
 * SPI.c
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

#include <Tasks_.h>
/****************************************SDK Included Files.*******************************************/
#include "MK66F18.h"
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "fsl_dspi.h"
#include "board.h"
#include "fsl_dspi_edma.h"
#include "fsl_edma.h"
#include "fsl_dmamux.h"
/**************************************FreeRTOS kernel includes.***************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"
/**************************************Local h-files includes.******************************************/
#include "DataLoggerProjectDEFINES.h"
#include "SPI.h"

/**************************************private attributes*****************************************/

static uint8_t*  					spi_receiveBufferPtr;						/*@brief DSPI pointer to the receive buffer.*/

static dspi_master_edma_handle_t 	spi_masterEdmaHandle;						/*@brief DSPI master edma handle.*/

static edma_handle_t 				spi_eDMAmasterRxRegToRxDataHandle;			/*@brief eDMA Rx_reg -> Rx transfer handle structure.*/
static edma_handle_t 				spi_eDMAmasterIntermediaryToTxRegHandle;	/*@brief eDMA Intermediary -> Tx transfer handle structure.*/
#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))	/*@brief If DSPI instances support Gasket feature, only two channels are needed.*/
static edma_handle_t 				spi_eDMAmasterTxDataToIntermediaryHandle;	/*@brief eDMA Tx -> Intermediary transfer handle structure.*/
#endif

static edma_config_t 				spi_eDMAconfiguration;						/*@brief eDMA configuration structure.*/

static uint32_t						spi_masterRxChannel;						/*@brief eDMA Rx master channel.*/
static uint32_t						spi_masterTxChannel;						/*@brief eDMA Tx master channel.*/
#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))	/*@brief If DSPI instances support Gasket feature, only two channels are needed.*/
static uint32_t 					spi_masterIntermediaryChannel;				/*@brief eDMA Intermediary master channel.*/
#endif

static dspi_master_config_t			spi_masterConfiguration;					/*@brief DSPI master configuration structure.*/

static dspi_transfer_t				spi_masterTransfer;							/*@brief DSPI master transfer structure.*/

uint32_t							spi_clockFrequencyHz;						/*@brief DSPI clock frequency variable to use in DSPI_MasterInit)*/

static volatile uint8_t 			spi_dataReceivedFlag;						/*@brief Flag to notify data has been received from SPI.*/

/**************************************private prototypes*****************************************/
/*
 * @brief DSPI master callback function.
 * @param SPI_Type *base 						-> DSPI peripheral base pointer.
 * @param dspi_master_edma_handle_t *edmaHandle	-> DSPI master EDMA handle pointer.
 * @param status_t status						-> Success or error code describing whether the transfer completed.
 * @param void *userData						-> An arbitrary pointer-dataSized value passed from the application.
 */
static void SPI_MasterCallbackFunction(SPI_Type *base, dspi_master_edma_handle_t *edmaHandle, status_t status, void *userData);

/***************	***********************private methods********************************************/
/*
 * @brief DSPI master callback function.
 * @param SPI_Type *base 						-> DSPI peripheral base pointer.
 * @param dspi_master_edma_handle_t *edmaHandle	-> DSPI master EDMA handle pointer.
 * @param status_t status						-> Success or error code describing whether the transfer completed.
 * @param void *userData						-> An arbitrary pointer-dataSized value passed from the application.
 */
static void SPI_MasterCallbackFunction(SPI_Type *base, dspi_master_edma_handle_t *edmaHandle, status_t status, void *userData)
{
	spi_dataReceivedFlag = TRUE;		/*@brief Let vTaskGetMeasurement know data is received and continue with its task.*/
}

/**************************************public methods*********************************************/
/*
 * @brief Initialisation of SPI.
 */
void SPI_initialisation(void)
{
	/*@brief Initialise receive buffer.*/
	spi_receiveBufferPtr = (uint8_t*) malloc(NO_OF_BYTES_ADC_AND_TIME_DIFF);
	memset(spi_receiveBufferPtr, '\0', NO_OF_BYTES_ADC_AND_TIME_DIFF);

	/*@brief DMA MUX initialisation*/
	spi_masterRxChannel = 0U;
	#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))
	spi_masterIntermediaryChannel = 2U;
	#endif
	spi_masterTxChannel = 1U;

	DMAMUX_Init(SPI_MASTER_DMA_MUX_BASEADDR);

	DMAMUX_SetSource(SPI_MASTER_DMA_MUX_BASEADDR, spi_masterRxChannel, (uint8_t) SPI_MASTER_DMA_RX_REQUEST_SOURCE);
	DMAMUX_EnableChannel(SPI_MASTER_DMA_MUX_BASEADDR, spi_masterRxChannel);
	DMAMUX_SetSource(SPI_MASTER_DMA_MUX_BASEADDR, spi_masterTxChannel, (uint8_t) SPI_MASTER_DMA_TX_REQUEST_SOURCE);
	DMAMUX_EnableChannel(SPI_MASTER_DMA_MUX_BASEADDR, spi_masterTxChannel);

	/*@brief EDMA initialisation*/
	EDMA_GetDefaultConfig(&spi_eDMAconfiguration);
	EDMA_Init(SPI_MASTER_DMA_BASEADDR, &spi_eDMAconfiguration);

	/*@brief DSPI initialisation*/
	spi_masterConfiguration.whichCtar									= kDSPI_Ctar0;

	spi_masterConfiguration.ctarConfig.baudRate							= SPI_TRANSFER_BAUDRATE;
	spi_masterConfiguration.ctarConfig.bitsPerFrame						= 8;
	spi_masterConfiguration.ctarConfig.cpha								= kDSPI_ClockPhaseFirstEdge;
	spi_masterConfiguration.ctarConfig.cpol								= kDSPI_ClockPolarityActiveHigh;
	spi_masterConfiguration.ctarConfig.direction						= kDSPI_MsbFirst;
	spi_masterConfiguration.ctarConfig.lastSckToPcsDelayInNanoSec		= 1000000000U / SPI_TRANSFER_BAUDRATE;
	spi_masterConfiguration.ctarConfig.betweenTransferDelayInNanoSec	= 1000000000U / SPI_TRANSFER_BAUDRATE;
	spi_masterConfiguration.ctarConfig.pcsToSckDelayInNanoSec			= 1000000000U / SPI_TRANSFER_BAUDRATE;

	spi_masterConfiguration.whichPcs									= SPI_MASTER_PCS_FOR_INIT;
	spi_masterConfiguration.pcsActiveHighOrLow							= kDSPI_PcsActiveLow;
	spi_masterConfiguration.enableContinuousSCK							= false;
	spi_masterConfiguration.enableModifiedTimingFormat					= false;
	spi_masterConfiguration.enableRxFifoOverWrite						= false;
	spi_masterConfiguration.samplePoint									= kDSPI_SckToSin0Clock;

	spi_clockFrequencyHz = SPI_MASTER_CLK_FREQ;
	DSPI_MasterInit(SPI_MASTER_BASEADDR, &spi_masterConfiguration, spi_clockFrequencyHz);

	/*@brief Initialise and create eDMA data handles.*/
	memset(&spi_eDMAmasterRxRegToRxDataHandle, '\0', sizeof(spi_eDMAmasterRxRegToRxDataHandle));
	memset(&spi_eDMAmasterIntermediaryToTxRegHandle, 0, sizeof(spi_eDMAmasterIntermediaryToTxRegHandle));
	#if (!(defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET))
		EDMA_CreateHandle(&spi_eDMAmasterTxDataToIntermediaryHandle, SPI_MASTER_DMA_BASEADDR, spi_masterIntermediaryChannel);
	#endif

	EDMA_CreateHandle(&spi_eDMAmasterRxRegToRxDataHandle, SPI_MASTER_DMA_BASEADDR, spi_masterRxChannel);
	EDMA_CreateHandle(&spi_eDMAmasterIntermediaryToTxRegHandle, SPI_MASTER_DMA_BASEADDR, spi_masterTxChannel);

	/*@brief Create DSPI master transfer handle.*/
	#if (defined(FSL_FEATURE_DSPI_HAS_GASKET) && FSL_FEATURE_DSPI_HAS_GASKET)
		DSPI_MasterTransferCreateHandleEDMA(SPI_MASTER_BASEADDR, &spi_masterEdmaHandle, SPI_MasterCallbackFunction, NULL, &spi_eDMAmasterRxRegToRxDataHandle, NULL, &spi_eDMAmasterIntermediaryToTxRegHandle);
	#else
		DSPI_MasterTransferCreateHandleEDMA(SPI_MASTER_BASEADDR, &spi_masterEdmaHandle, SPI_MasterCallbackFunction, NULL, &spi_eDMAmasterRxRegToRxDataHandle, &spi_eDMAmasterTxDataToIntermediaryHandle, &spi_eDMAmasterIntermediaryToTxRegHandle);
	#endif

	/*@brief As this SPI driver configuration will only receive data, master transfer can be configured here.*/
	spi_masterTransfer.txData		= NULL;
	spi_masterTransfer.rxData		= spi_receiveBufferPtr;
	spi_masterTransfer.dataSize		= NO_OF_BYTES_FROM_ADC;
	spi_masterTransfer.configFlags	= kDSPI_MasterCtar0 | SPI_MASTER_PCS_FOR_TRANSFER | kDSPI_MasterPcsContinuous;
}
/*
 * @brief Getter for the receive buffer.
 * @return uint8_t* dspi_receiveBuffer	-> The DSPI receive buffer.
 */
uint8_t* SPI_getReceiveBuffer()
{
	return spi_receiveBufferPtr;
}
/*
 * @brief DSPI receive request for new measurement data.
 */
void SPI_getNewmeasurementData()
{
	DSPI_MasterTransferEDMA(SPI_MASTER_BASEADDR, &spi_masterEdmaHandle, &spi_masterTransfer);
}
/*
 * @brief Getter forspi_dataReceivedFlag pointer.
 */
volatile uint8_t* SPI_get_dataReceivedFlag()
{
	return &spi_dataReceivedFlag;
}
