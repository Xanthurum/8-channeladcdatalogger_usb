/*
 * TimersAndCo.c
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

/****************************************SDK Included Files.*******************************************/
#include "board.h"
#include "clock_config.h"
#include "MK66F18.h"
#include "fsl_pit.h"
/**************************************FreeRTOS kernel includes.***************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"
/**************************************Local h-files includes.******************************************/
#include "DataLoggerProjectDEFINES.h"
#include "TimersAndCo.h"
#include <Tasks_.h>
/**************************************private attributes*****************************************/

static uint64_t 			lastTimerValue;						/*@brief The last timer value, used to calculate the time difference.*/

static pit_config_t			timersCo_PIT_config;				/*@brief The configuration structure for the PIT module, Periodic Interrupt Timer (PIT).*/

static volatile uint8_t timeIntervalPassedFlag;
static volatile uint8_t delay_100ns_PassedFlag;
static volatile uint8_t delay_MS_Flag;

/**************************************private prototypes*****************************************/
/*
 * @brief Initialisation of a PIT for a 100ns delay.
 */
static void TimersAndCo_init_100ns_timer_PIT();
/*
 * @brief Initialisation of a PIT for the measurement interval.
 */
static void TimersAndCo_init_measurementInterval_PIT(void);
/*
 * @brief Initialisation of a PIT for the time difference.
 */
static void TimersAndCo_init_timeDifference_PIT(void);
/*
 * @brief Initialisation of a PIT for the millisecond timer.
 */
void TimersAndCo_init_MS_timer_PIT(void);
/**************************************private methods********************************************/
/*
 * @brief Initialisation of a PIT for a 100ns delay.
 */
static void TimersAndCo_init_100ns_timer_PIT()
{
	PIT_SetTimerPeriod(PIT, PIT_100NS_TIMER_PIT_CHANNEL, PIT_100NS_TIMER_COUNT);
	PIT_EnableInterrupts(PIT, PIT_100NS_TIMER_PIT_CHANNEL, kPIT_TimerInterruptEnable);
	NVIC_SetPriority(PIT_100NS_TIMER_IRQ_ID, PIT_100NS_TIMER_IRQ_ID);
	EnableIRQ(PIT_100NS_TIMER_IRQ_ID);
}
/*
 * @brief Initialisation of a PIT for the measurement interval.
 */
static void TimersAndCo_init_measurementInterval_PIT(void)
{
	PIT_SetTimerPeriod(PIT, PIT_MEASUREMENT_PIT_CHANNEL, USEC_TO_COUNT(PIT_MEASUREMENT_PERIOD_US, PIT_SOURCE_CLOCK));
	PIT_EnableInterrupts(PIT, PIT_MEASUREMENT_PIT_CHANNEL, kPIT_TimerInterruptEnable);
	NVIC_SetPriority(PIT_MEASUREMENT_IRQ_ID, PIT_MEASUREMENT_IRQ_ID);
	EnableIRQ(PIT_MEASUREMENT_IRQ_ID);
	PIT_StartTimer(PIT, PIT_MEASUREMENT_PIT_CHANNEL);
}
/*
 * @brief Initialisation of a PIT for the time difference.
 */
static void TimersAndCo_init_timeDifference_PIT(void)
{
	PIT_SetTimerPeriod(PIT, PIT_TIME_DIFFERENCE_CHANNEL, MSEC_TO_COUNT(PIT_TIME_DIFFERENCE_MS, PIT_SOURCE_CLOCK));
	PIT_EnableInterrupts(PIT, PIT_TIME_DIFFERENCE_CHANNEL, kPIT_TimerInterruptEnable);
	NVIC_SetPriority(PIT_TIME_DIFFERENCE_IRQ_ID, PIT_TIME_DIFFERENCE_IRQ_ID);
	EnableIRQ(PIT_TIME_DIFFERENCE_IRQ_ID);
	PIT_StartTimer(PIT, PIT_TIME_DIFFERENCE_CHANNEL);
}
/*
 * @brief Initialisation of a PIT for the millisecond timer.
 */
void TimersAndCo_init_MS_timer_PIT(void)
{
	PIT_SetTimerPeriod(PIT, PIT_MS_TIMER_PIT_CHANNEL, MSEC_TO_COUNT(PIT_MS_TIMER_PERIOD_MS, PIT_SOURCE_CLOCK));
	PIT_EnableInterrupts(PIT, PIT_MS_TIMER_PIT_CHANNEL, kPIT_TimerInterruptEnable);
	NVIC_SetPriority(PIT_MS_TIMER_IRQ_ID, PIT_MS_TIMER_IRQ_ID);
	EnableIRQ(PIT_MS_TIMER_IRQ_ID);
}
/**************************************public methods*********************************************/
/*
 * @brief Initialisation of all timers and co.
 */
void TimersAndCo_initialisation(void)
{
	/*
	 * @brief initialise local variables.
	 */
	timeIntervalPassedFlag 	= 0;
	delay_100ns_PassedFlag 	= 0;
	delay_MS_Flag 	= 0;
	lastTimerValue 			= 1000000;				/*@brief initialise last timer value.*/
	intervalCount 			= 0;
	/*
	 * @brief Initialisation of the PIT module and its channels.
	 */
	PIT_GetDefaultConfig(&timersCo_PIT_config);
	timersCo_PIT_config.enableRunInDebug = TRUE;
	PIT_Init(PIT, &timersCo_PIT_config);

	TimersAndCo_init_100ns_timer_PIT();

	TimersAndCo_init_measurementInterval_PIT();

	TimersAndCo_init_timeDifference_PIT();

	TimersAndCo_init_MS_timer_PIT();
}
/*
 * @brief Function to get the time difference.
 * @return uint16_t µs_difference	-> The time difference in µs.
 */
uint16_t TimersAndCo_GetTimeDifference(void)
{
//	uint64_t currentTimerValue	= COUNT_TO_USEC(PIT_GetCurrentTimerCount(PIT, PIT_TIME_DIFFERENCE_CHANNEL), PIT_SOURCE_CLOCK);
//	uint64_t timeDifference 	= lastTimerValue - currentTimerValue;
//	lastTimerValue				= currentTimerValue;
//
//	return (uint16_t)timeDifference;
	return intervalCount;
}
/*
 * @brief Function to get the current timer value.
 * @return uint64_t µs_time	-> The timer value in µs.
 */
uint64_t TimersAndCo_GetCurrentTimerValue(void)
{
	uint64_t currentTimerValue	= COUNT_TO_USEC(PIT_GetCurrentTimerCount(PIT, PIT_TIME_DIFFERENCE_CHANNEL), PIT_SOURCE_CLOCK);

	return currentTimerValue;
}
/*
 * @brief Function to get the TimeIntervalPassedFlag pointer.
 * @return uint8_t* timeIntervalPassedFlagPtr
 */
volatile uint8_t* TimersAndCo_GetTimeIntervalPassedFlagPtr(void)
{
	return &timeIntervalPassedFlag;
}
/*
 * @brief Function to start the 100ns delay.
 */
void TimersAndCo_Start_100ns_delay(void)
{
	PIT_StartTimer(PIT, PIT_100NS_TIMER_PIT_CHANNEL);
}
/*
 * @brief Function to get the delay_100ns_PassedFlag pointer.
 * @return uint8_t* delay_100ns_PassedFlagPtr
 */
volatile uint8_t* TimersAndCo_GetDelay_100ns_PassedFlagPtr(void)
{
	return &delay_100ns_PassedFlag;
}
/*
 * @brief Function to start a delay in milliseconds.
 * @param uint64_t timeInMilliseconds	-> The milliseconds timer value to set.
 */
void TimersAndCo_Start_MS_delay(uint64_t timeInMilliseconds)
{
	PIT_SetTimerPeriod(PIT, PIT_MS_TIMER_PIT_CHANNEL, MSEC_TO_COUNT((uint64_t)timeInMilliseconds, PIT_SOURCE_CLOCK));
	PIT_StartTimer(PIT, PIT_MS_TIMER_PIT_CHANNEL);
	delay_MS_Flag = 1;
}
/*
 * @brief Function to get the delay_MS_Flag pointer.
 * @return uint8_t* delay_MS_FlagPtr
 */
volatile uint8_t* TimersAndCo_GetDelay_MS_FlagPtr(void)
{
	return &delay_MS_Flag;
}

/**************************************IRQ Handlers*********************************************/
/*
 * @brief Interrupt service handler for the millisecond timer event.
 */
void PIT_MS_TIMER_HANDLER(void)
{
	PIT_ClearStatusFlags(PIT, PIT_MS_TIMER_PIT_CHANNEL, kPIT_TimerFlag);
	PIT_StopTimer(PIT, PIT_MS_TIMER_PIT_CHANNEL);
	delay_MS_Flag = 0;
}
/*
 * @brief Interrupt service handler for the sdmmc timer event.
 */
void PIT_100NS_TIMER_HANDLER(void)
{
	PIT_ClearStatusFlags(PIT, PIT_100NS_TIMER_PIT_CHANNEL, kPIT_TimerFlag);
	PIT_StopTimer(PIT, PIT_100NS_TIMER_PIT_CHANNEL);
	delay_100ns_PassedFlag = 1;
}
/*
 * @brief Interrupt service handler for the measurement interval.
 */
void PIT_MEASUREMENT_HANDLER(void)
{
	PIT_ClearStatusFlags(PIT, PIT_MEASUREMENT_PIT_CHANNEL, kPIT_TimerFlag);

	timeIntervalPassedFlag = 1;
	intervalCount++;
	if(intervalCount == 0xFF) intervalCount = 0;
}
/*
 * @brief Interrupt service handler for the time difference timer.
 */
void PIT_TIME_DIFFERENCE_HANDLER(void)
{
	PIT_ClearStatusFlags(PIT, PIT_TIME_DIFFERENCE_CHANNEL, kPIT_TimerFlag);

	lastTimerValue += (PIT_TIME_DIFFERENCE_MS * 1000);
}
