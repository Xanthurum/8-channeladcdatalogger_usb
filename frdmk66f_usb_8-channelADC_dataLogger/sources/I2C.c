/*
 * I2C.c
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

/****************************************SDK Included Files.*******************************************/
#include "board.h"
#include "fsl_debug_console.h"
#include "fsl_i2c.h"
#include "MK66F18.h"
/**************************************FreeRTOS kernel includes.***************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
/**************************************Local c-files includes.******************************************/
#include "I2C.h"
#include "Tasks_.h"
#include "TimersAndCo.h"
/**************************************private attributes*****************************************/

    /*
     * @brief i2c_master_config_t						-> I2C master transfer structure.
     * @param bool enableMaster	default:true			-> Enables the I2C peripheral at initialisation time.
     * @param bool enableStopHold default:false			-> Controls the stop hold enable.
     * @param bool enableDoubleBuffering default:true	-> Controls double buffer enable; notice that enabling the double buffer disables the clock stretch.
     * @param uint32_t baudRate_Bps default:100000U		-> Baud rate configuration of I2C peripheral.
     * @param uint8_t glitchFilterWidth default:0U		-> Controls the width of the glitch.
     */
    static i2c_master_config_t masterConfig;
    /*
     * @brief i2c_master_transfer_t		-> I2C master transfer structure.
     * @param uint32_t flags			-> A transfer flag which controls the transfer.
     * @param uint8_t slaveAddress		-> 7-bit slave address.
     * @param i2c_direction_t direction	-> A transfer direction, aka kI2C_Write or kI2C_Read.
     * @param uint32_t sub address		-> A sub address. Transferred MSB first.
     * @param uint8_t subaddressSize	-> A size of the command buffer.
     * @param uint8_t *volatile data	-> A transfer buffer.
     * @param volatile size_t dataSize	-> A transfer size.
     */
    static i2c_master_transfer_t masterTransfer;
    /*
     * @brief I2C master handle
     * @param i2c_master_transfer_t transfer                    ->	 I2C master transfer copy.
     * @param size_t transferSize                               ->	 Total bytes to be transferred.
     * @param uint8_t state                                     ->	 A transfer state maintained during transfer.
     * @param i2c_master_transfer_callback_t completionCallback -> 	 A callback function called when the transfer is finished.
     * @param void *userData                                    ->	 A callback parameter passed to the callback function.
     */
    static i2c_master_handle_t i2cMasterHandle;
    /*
     * @brief I2C semaphore handle.
     */
    SemaphoreHandle_t i2cSemaphore;
    /*
     * @brief char pointer to master transfer buffer
     */
    static char *masterBuffer;
    static uint8_t masterAddress;
    static status_t status;
    /*
     * @brief pointer to I2C base address.
     */
    I2C_Type *i2cBase;

/**************************************private prototypes*****************************************/

    /*
     * @brief master callback function
     * @param I2C_Type *base				-> I2C base address.
     * @param i2c_master_handle_t *handle	-> I2C master handle.
     * @param status_t status				-> I2C status return codes.
     * @param void *userData				-> I2C received data
     */
    static void i2cMasterCallback(I2C_Type *base, i2c_master_handle_t *i2cMaster_handle, status_t status, void *userData);

/**************************************private methods*********************************************/

    /*
     * @brief master callback function
     * @param I2C_Type *base				-> I2C base address.
     * @param status_t status				-> I2C status return codes.
     * @param i2c_master_handle_t *handle	-> I2C master handle.
     * @param void *userData				-> I2C received data
     */
    void i2cMasterCallback(I2C_Type *base, i2c_master_handle_t *i2cMaster_handle, status_t status, void *userData)
    {
    		//xSemaphoreGive(get_xI2CdataReceivedSemaphore();
    }

/**************************************public methods*********************************************/

    /*
     * @brief Initialisation for an i2c peripheral on the NXP-MK66F => initialisation of I2C1.
     * @param uint_32_t baudrate			-> Baudrate i2c, ex. 100000 aka 100kHz.
     * @param IRQn_Type irqnOfTheI2Cbase	-> IRQn of the I2C peripheral, ex. I2C1_IRQn.
     * @param I2C_Type* base				-> Base of the I2C peripheral, ex. I2C1.
     * @param clock_name_t clockName		-> Clockname of the I2C peripheral, ex. I2C1_CLK_SRC.
     * @param uint8_t masterAddress7bit		-> Master address of the I2C peripheral, ex. 0x01U
     */
    void I2C_initialisation(uint32_t baudrate, IRQn_Type irqnOfTheI2Cbase, I2C_Type* base, clock_name_t clockName, uint8_t masterAddress7bit)
    {
    	i2cBase = base;
    	masterAddress = masterAddress7bit;

    	/*@brief Set priority IRQ for I2C peripheral.*/
    	NVIC_SetPriority(irqnOfTheI2Cbase, irqnOfTheI2Cbase);

    	/*@brief Config SDK i2c driver.*/
        I2C_MasterGetDefaultConfig(&masterConfig);
        masterConfig.baudRate_Bps = baudrate;

        uint32_t sourceClock = CLOCK_GetFreq(clockName);

        /*@brief Initialise i2c driver.*/
        I2C_MasterInit(i2cBase, &masterConfig, sourceClock);

        /*@brief Initialise masterTransfer & i2cMasterHandle to 0.*/
        memset(&masterTransfer, 0, sizeof(masterTransfer));
        memset(&i2cMasterHandle, 0, sizeof(i2cMasterHandle));

        /*@brief  Initialises the I2C handle which is used in transactional functions.*/
        I2C_MasterTransferCreateHandle(i2cBase, &i2cMasterHandle, i2cMasterCallback, NULL);

    }
    /*
     * @brief getter for master transfer struct
     * @return i2c_master_transfer_t masterTransfer
     */
    i2c_master_transfer_t* I2C_GetMasterTransfer(void)
    {
    	return &masterTransfer;
    }
    /*
     * @brief getter for master status
     * @return status_t masterStatus
     */
    status_t I2C_GetMasterStatus(void)
    {
    	return status;
    }
    /*
     * @brief I2C master transfer function.
     * @param uint8_t* bytesPtr			-> Pointer to the bytes to transfer.
     * @param uint8_t slaveAddress		-> Slave address.
     * @param i2c_direction_t direction	-> A transfer direction, aka kI2C_Write or kI2C_Read.
     * @return status_t status			-> Status of the transfer, kStatus_Success.
     */
    status_t I2C_MasterTransfer(uint8_t* bytesPtr, uint8_t slaveAddress, i2c_direction_t direction)
    {
    	masterTransfer.data 		= bytesPtr;
    	masterTransfer.slaveAddress	= slaveAddress;
    	masterTransfer.direction	= direction;
    	masterTransfer.flags		= kI2C_TransferDefaultFlag;

		return status = I2C_MasterTransferNonBlocking(i2cBase, &i2cMasterHandle, &masterTransfer);
    }







