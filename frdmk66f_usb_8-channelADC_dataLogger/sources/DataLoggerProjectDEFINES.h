/*
 * DataLoggerProjectDEFINES.h
 *
 *  Created on: 13 Apr 2018
 *      Author: Pascal Barbary
 */

#ifndef DATALOGGERPROJECTDEFINES_H_
#define DATALOGGERPROJECTDEFINES_H_

/**************************************Global variables************************************************/
#ifdef GLOBAL_VAR
#define GLOBAL
#else
#define GLOBAL extern
#endif
GLOBAL uint16_t intervalCount;

/**************************************Defines I2C*****************************************************/

#define MAX_SIZE_DATA_I2C 					32												/*The max data length in bytes the i2c driver should handle*/

/**************************************Defines SD_card*************************************************/

#define BUFFER_SIZE 						(FSL_SDMMC_DEFAULT_BLOCK_SIZE * 1)				/*Buffer size (in byte) for read/write operations, multiple of 512.*/
#define MAX_BUFFERS_IN_FILE					(40000 / BUFFER_SIZE)							/*The maximum # buffers that can be written in a file with a maximum size of 4GB.*/
#define FILE_READ 							(FA_OPEN_ALWAYS | FA_READ | FA_OPEN_EXISTING)	/*Access mode and file open mode flags to read a file*/
#define FILE_CREATE 						(FA_WRITE | FA_READ | FA_CREATE_ALWAYS)			/*Access mode and file open mode flags to create a file*/
#define NO_OF_BYTE_IN_ROW					56												/*The total number of bytes in a row of measurement data, ',' & '\r' & '\n' included.*/
#define NO_OF_BYTE_MEASURE_DATA 			18												/*The net total number of bytes in a row of measurement data.*/
#define NO_OF_BYTE_CONFIG_DATA				8												/*The net total number of bytes in a row of configuration data*/
#define MAX_NO_OF_BYTE_CONFIG_DATA_FILE		22												/*The maximum number of bytes in a row of configuration data read from the configuration file, ',' included + 1.*/
#define DIR_PATH_MEASURE					"/Measure"										/*The directory path to Measure.*/
#define FILE_PATH_MEASURE					"/Measure/file"									/*The file path for a file without the file extension.*/
#define NO_OF_BYTES_FILE_PATH_MEASURE		13												/*@brief The number of bytes of FILE_PATH_MEASURE aka the place where to put the number in de file path name.*/
#define FILE_EXTENSION						".txt"											/*The file extension.*/
#define DIR_PATH_CONFIG						"/Config"										/*The directory path to Config.*/
#define FILE_PATH_CONFIG					"/Config/config.txt"							/*The file path for the configuration file.*/

/**************************************Defines SPI*****************************************************/

#define SPI_MASTER_BASEADDR 				SPI0											/*@brief DSPI peripheral base pointer.*/
#define SPI_MASTER_DMA_MUX_BASEADDR 		DMAMUX											/*@brief DMAMUX peripheral base pointer.*/
#define SPI_MASTER_DMA_BASEADDR 			DMA0											/*@brief DMA peripheral base pointer.*/
#define SPI_MASTER_DMA_RX_REQUEST_SOURCE 	kDmaRequestMux0SPI0Rx							/*@brief DMA Rx hardware request for SPI0.*/
#define SPI_MASTER_DMA_TX_REQUEST_SOURCE 	kDmaRequestMux0SPI0Tx							/*@brief DMA Tx hardware request for SPI0.*/
#define SPI_MASTER_CLK_SRC 					DSPI0_CLK_SRC									/*@brief DSPI clock source, aka kCLOCK_BusClk.*/
#define SPI_MASTER_CLK_FREQ 				CLOCK_GetFreq(DSPI0_CLK_SRC)					/*@brief DSPI clock frequency.*/
#define SPI_MASTER_PCS_FOR_INIT 			kDSPI_Pcs0										/*@brief DSPI peripheral chip select configuration.*/
#define SPI_MASTER_PCS_FOR_TRANSFER 		kDSPI_MasterPcs0								/*@brief DSPI master transfer use PCS0 chip select signal, aka PIN PTD0.*/
#define NO_OF_BYTES_FROM_ADC				16        										/*@brief The number of bytes to receive from the ADC.*/
#define NO_OF_BYTES_ADC_AND_TIME_DIFF		18												/*@brief The number of bytes to receive from the ADC + the no of bytes for the time difference.*/
#define SPI_TRANSFER_BAUDRATE 				30000000U 										/*@brief DSPI baudrate, aka 30.000.000.*/

/**************************************Defines Tasks***************************************************/

#define HIGH								1
#define LOW									0
#define TRUE								1
#define FALSE								0

#define PIN_AS_INPUT						{kGPIO_DigitalInput, 0}							/*@brief PIN as input definition.*/
#define PIN_AS_OUTPUT						{kGPIO_DigitalOutput, 0}						/*@brief PIN as input definition.*/

#define ADC_OS0_PORT						GPIOE											/*@brief PIN define port for ADC OS0.*/
#define ADC_OS0_PIN						    25U												/*@brief PIN define pin for ADC OS0.*/
#define ADC_OS1_PORT						GPIOD											/*@brief PIN define port for ADC OS1.*/
#define ADC_OS1_PIN							13U												/*@brief PIN define pin for ADC OS1.*/
#define ADC_OS2_PORT						GPIOD											/*@brief PIN define port for ADC OS2.*/
#define ADC_OS2_PIN							12U												/*@brief PIN define pin for ADC OS2.*/
#define ADC_RANGE_PORT						GPIOB											/*@brief PIN define port for ADC RANGE.*/
#define ADC_RANGE_PIN						11U												/*@brief PIN define pin for ADC RANGE.*/
#define ADC_CONVST_PORT						GPIOC											/*@brief PIN define port for ADC CONVST.*/
#define ADC_CONVST_PIN						0U												/*@brief PIN define pin for ADC CONVST.*/
#define ADC_BUSY_PORT						GPIOA											/*@brief PIN define port for ADC BUSY.*/
#define ADC_BUSY_PIN						27U												/*@brief PIN define pin for ADC BUSY.*/
#define ADC_BUSY_INTERRUPT_IRQn				PORTA_IRQn										/*@brief PIN define interrupt IRQn for ADC BUSY.*/
#define ADC_BUSY_INTERRUPT_PORT				PORTA											/*@brief PIN define interrupt port for ADC BUSY.*/
#define ADC_BUSY_INTERRUPT_Type				kPORT_InterruptFallingEdge						/*@brief PIN define interrupt type for ADC BUSY.*/
#define ADC_FRST_DATA_PORT					GPIOA											/*@brief PIN define port for ADC FRST_DATA.*/
#define ADC_FRST_DATA_PIN					26U												/*@brief PIN define pin for ADC FRST_DATA.*/
#define ADC_RESET_PORT						GPIOE											/*@brief PIN define port for ADC RESET.*/
#define ADC_RESET_PIN						24U												/*@brief PIN define pin for ADC RESET.*/
#define PCB_LED_PORT						GPIOB											/*@brief PIN define port for PCB led.*/
#define PCB_LED_PIN							7U												/*@brief PIN define pin for PCB led.*/
#define RGB_LED_RED_PORT					GPIOC											/*@brief PIN define port for FRDM-K66F RGB led red.*/
#define RGB_LED_RED_PIN						9U												/*@brief PIN define pin for FRDM-K66F RGB led red.*/

#define FILTER_CLOCK_PORT					GPIOA											/*@brief PIN define port for clock input filter ADC.*/
#define FILTER_CLOCK_PIN					8U												/*@brief PIN define pin for clock input filter ADC.*/
#define BUTTON_START_MEASURE_PORT			GPIOC											/*@brief PIN define port for button start measurement.*/
#define BUTTON_START_MEASURE_PIN			1U												/*@brief PIN define pin for button start measurement.*/
#define BUTTON_START_MEA_INTERRUPT_PORT		PORTC											/*@brief PIN define interrupt port for button start measurement.*/
#define BUTTON_START_MEA_INTERRUPT_IRQn		PORTC_IRQn										/*@brief PIN define interrupt IRQn for button start measurement.*/
#define BUTTON_START_MEA_INTERRUPT_Type		kPORT_InterruptFallingEdge						/*@brief PIN define interrupt type for button start measurement..*/

#define GAIN0_I2C_SLAVE_ADD 				0x42											/*@brief Gain0 i2c slave address.*/
#define GAIN1_I2C_SLAVE_ADD 				0x46											/*@brief Gain1 i2c slave address.*/
#define GAIN2_I2C_SLAVE_ADD 				0x44											/*@brief Gain2 i2c slave address.*/
#define GAIN3_I2C_SLAVE_ADD 				0x48											/*@brief Gain3 i2c slave address.*/

/**************************************Defines TimersAndCo*********************************************/

#define PIT_SOURCE_CLOCK					CLOCK_GetFreq(kCLOCK_BusClk)					/*@brief PIT source clock.*/

#define PIT_MS_TIMER_PERIOD_MS				50U												/*@brief Period in ms for the MS event timer.*/
#define PIT_MS_TIMER_HANDLER				PIT0_IRQHandler									/*@brief PIT IRQ handler for the MS event timer.*/
#define PIT_MS_TIMER_IRQ_ID					PIT0_IRQn										/*@brief PIT IRQ ID for the MS event timer.*/
#define PIT_MS_TIMER_PIT_CHANNEL			kPIT_Chnl_0										/*@brief PIT channel for the MS event timer.*/

#define PIT_100NS_TIMER_COUNT				5U												/*@brief Timer count for 100ns, formula (period/period bus clock)-1.Aka (100ns/(1/60E6))-1.*/
#define PIT_100NS_TIMER_HANDLER				PIT1_IRQHandler									/*@brief PIT IRQ handler for the 100ns timer.*/
#define PIT_100NS_TIMER_IRQ_ID				PIT1_IRQn										/*@brief PIT IRQ ID for the 100ns timer.*/
#define PIT_100NS_TIMER_PIT_CHANNEL			kPIT_Chnl_1										/*@brief PIT channel for the 100ns timer.*/

#define PIT_MEASUREMENT_PERIOD_COUNT		200U											/*@brief Timer count for 10µs measurement interval, formula (period/period bus clock)-1.Aka (10µs/(1/60E6))-1.*/
#define PIT_MEASUREMENT_PERIOD_US			10U												/*@brief Period in µs for the measurement interval.*/
#define PIT_MEASUREMENT_HANDLER				PIT2_IRQHandler									/*@brief PIT IRQ handler for the measurement interval.*/
#define PIT_MEASUREMENT_IRQ_ID				PIT2_IRQn										/*@brief PIT IRQ ID for the measurement interval timer.*/
#define PIT_MEASUREMENT_PIT_CHANNEL			kPIT_Chnl_2										/*@brief PIT channel for the measurement interval timer.*/

#define PIT_TIME_DIFFERENCE_COUNT			60000000U										/*@brief Timer count for 1s, formula (period/period bus clock)-1.Aka (1000ms/(1/60E6))-1.*/
#define PIT_TIME_DIFFERENCE_MS				1000U											/*@brief The timer value in ms used to calculate the time difference between measurements.*/
#define PIT_TIME_DIFFERENCE_HANDLER			PIT3_IRQHandler									/*@brief PIT IRQ handler for the time difference timer.*/
#define PIT_TIME_DIFFERENCE_IRQ_ID			PIT3_IRQn										/*@brief PIT IRQ ID for the time difference timer.*/
#define PIT_TIME_DIFFERENCE_CHANNEL			kPIT_Chnl_3										/*@brief PIT channel for the time difference timer.*/

#endif /* DATALOGGERPROJECTDEFINES_H_ */
