/*
 * Tasks.c
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

/****************************************SDK Included Files.*******************************************/
#include "board.h"
#include "fsl_port.h"
#include "fsl_debug_console.h"
#include "fsl_i2c.h"
#include "MK66F18.h"
/**************************************Local h-files includes*******************************************/
#include "DataLoggerProjectDEFINES.h"
#include "I2C.h"
#include "SPI.h"
#include "TimersAndCo.h"
#include "SD_card.h"
#include "Tasks_.h"
#include "virtual_com.h"
#include "Conversions.h"
#include "usb_device_cdc_acm.h"
/**************************************private attributes*****************************************/

static xQueueHandle 	xConfigDataQueue;						/*@brief Handle for the configuration data queue.*/

static volatile uint8_t ADC_busyFlag;							/*@brief Busy flag of ADC BUSY pin.*/
static volatile uint8_t startButtonPushedFlag;					/*@brief Flag of the start button.*/
static volatile uint8_t firstConfigDoneFlag;					/*@brief Flag to signal if the configuration of the ADC has been done.*/
static uint32_t 		tasks_interruptFlags;					/*@brief Flag to read interrupts flags from a GPIO port.*/

/**************************************private prototypes*****************************************/
/*
 * @brief Task to get a new measurement from the ADC via spi, add a form of timestamp and write the data to a host pc via USB.
 */
static void vTaskGetMeasurement(void *pvParameters);
/*
 * @brief Task to set the new configuration data received from USB.
 * @param void* configDataPtr.
 */
static void vTaskSetNewConfigData(void *pvParameters);
/*
 * @brief Task to initialise the system at the start.
 */
static void vTaskInitSystem(void *pvParameters);
/*
 * @brief Task to toggle PCB_LED & RGB_LED_RED.
 * @param uint64_t intervalMilliseconds
 */
static void vTaskToggle_LED(uint64_t intervalMilliseconds);
/*
 * @brief Task to set PCB_LED & RGB_LED_RED LOW.
 */
static void vTaskClear_LED(void);

/**************************************IRQ Handlers*********************************************/
/*
 * @brief Interrupt service handler for GPIO port A, for ADC busy.
 */
void PORTA_IRQHandler(void)
{
	tasks_interruptFlags = GPIO_PortGetInterruptFlags(ADC_BUSY_PORT);							/*@brief Get GPIOA interrupt flags.*/

	if(((tasks_interruptFlags >> ADC_BUSY_PIN) & 0x1) == true)
	{
		GPIO_PortClearInterruptFlags(ADC_BUSY_PORT,1 << ADC_BUSY_PIN);
		ADC_busyFlag = 0;																		/*@brief free ADC_busyFlag so vTaskGetMeasurement can continue its task.*/
	}
}
/*
 * @brief Interrupt service handler for GPIO port C, for button start.
 */
void PORTC_IRQHandler(void)
{
	tasks_interruptFlags = GPIO_PortGetInterruptFlags(BUTTON_START_MEASURE_PORT);				/*@brief Get GPIOC interrupt flags.*/

	if(((tasks_interruptFlags >> BUTTON_START_MEASURE_PIN) & 0x1) == true)
	{
		GPIO_PortClearInterruptFlags(BUTTON_START_MEASURE_PORT, 1 << BUTTON_START_MEASURE_PIN);
		startButtonPushedFlag = 1;																/*@brief set startButtonPushedFlag so Tasks_initialisation can continue its task.*/
		printf("button start is pushed");
	}
}
/**************************************private methods********************************************/

/*
 * @brief Task to get a new measurement from the ADC via spi, add a form of timestamp and write the data to a host pc via USB.
 */
static void vTaskGetMeasurement(void *pvParameters)
{
	/*@brief Initialising vTaskGetMeasurement localBytePointers.*/
	uint8_t* rawMeasureData_localBytePtr = (uint8_t*) pvPortMalloc(NO_OF_BYTES_ADC_AND_TIME_DIFF);
	memset(rawMeasureData_localBytePtr, '\0', NO_OF_BYTES_ADC_AND_TIME_DIFF);
	uint8_t* rawMeasureData_localBytePtrAdd = rawMeasureData_localBytePtr;														/*@brief Get the address of the local byte pointer to easily reset tasks_localBytePointer back to its first byte.*/

	uint8_t* convertedMeasureData_localBytePtr = (uint8_t*) pvPortMalloc(NO_OF_BYTE_IN_ROW);
	memset(convertedMeasureData_localBytePtr, '\0', NO_OF_BYTE_IN_ROW);


	uint8_t* newConfigData_localBytePtr = (uint8_t*) pvPortMalloc(NO_OF_BYTE_IN_ROW);
	memset(newConfigData_localBytePtr, '\0', NO_OF_BYTE_IN_ROW);
	uint8_t* newConfigData_localBytePtrAdd = newConfigData_localBytePtr;														/*@brief Get the address of the local byte pointer to easily reset tasks_localBytePointer back to its first byte.*/

	/*@brief Initialising local variables for USB & SPI communication and timers.*/
	usb_cdc_vcom_struct_t* usb_cdcVcomPtr 		= USB_get_vcomStructPtr();
	uint8_t* usb_sendBufferPtr					= USB_get_sendBufferPtr();
	uint8_t* usb_receiveBufferPtr				= USB_get_reveiveBufferPtr();
	volatile uint32_t* usb_sendBufferSizePtr	= USB_get_sendSizePtr();
	volatile uint32_t* usb_receiveBufferSizePtr	= USB_get_reveiveSizePtr();
	volatile uint8_t* timeIntervalPassedFlag	= TimersAndCo_GetTimeIntervalPassedFlagPtr();
	volatile uint8_t* delay_100ns_PassedFlag	= TimersAndCo_GetDelay_100ns_PassedFlagPtr();
	volatile uint8_t* spi_dataReceivedFlag		= SPI_get_dataReceivedFlag();

	/*@brief endless loop to take measurements.*/
	for(;;)
	{
		usb_status_t error = kStatus_USB_Error;

		/*
		 * @brief Wait until the time interval between measurements has passed.
		 */
		if(*timeIntervalPassedFlag == TRUE)
		{
			/*
			 * @brief If a new configuration is received, it gets handled with the highest priority!
			 */
            if ((*usb_receiveBufferSizePtr != 0) && (*usb_receiveBufferSizePtr != 0xFFFFFFFF))
            {

        		printf("Config data received, total bytes = %d.\n", *usb_receiveBufferSizePtr);

                /* Copy Buffer to Send Buff */
                memcpy(newConfigData_localBytePtr, usb_receiveBufferPtr, NO_OF_BYTE_IN_ROW);
                *usb_receiveBufferSizePtr = 0;
                xQueueSend(xConfigDataQueue, newConfigData_localBytePtr, 0);
            }

			/*
			 * @brief If no new configuration is received, a new measurement gets done! But only if the ADC configuration has been done at the start.
			 */
			memset(rawMeasureData_localBytePtr, '\0', NO_OF_BYTES_ADC_AND_TIME_DIFF);
			memset(convertedMeasureData_localBytePtr, '\0', NO_OF_BYTE_IN_ROW);

			int i;
			for(i = 0; i < NO_OF_BYTES_ADC_AND_TIME_DIFF; i++)
			{
				*rawMeasureData_localBytePtr++ = 255;
			}
			rawMeasureData_localBytePtr = rawMeasureData_localBytePtrAdd;

			if(firstConfigDoneFlag == TRUE)
			{
				/*@Brief Signal the ADC to start a new conversion, shortly set the CONVST pin high.*/
				GPIO_PortToggle(ADC_CONVST_PORT, 1 << ADC_CONVST_PIN);										/*@brief Signal the ADC to start a new conversion, set the CONVST pin high.*/
				TimersAndCo_Start_100ns_delay();
				while(!*delay_100ns_PassedFlag) {}															/*@brief Wait for 100ns.*/
				*delay_100ns_PassedFlag = 0;
				GPIO_PortToggle(ADC_CONVST_PORT, 1 << ADC_CONVST_PIN);										/*@brief Signal the ADC to start a new conversion, set the CONVST pin low.*/

				/*
				 * Uncomment the following if SPI communication is connected.*************************************
				 */
//				while(GPIO_PinRead(ADC_BUSY_PORT, ADC_BUSY_PIN) == TRUE) {}									/*@brief Wait until the ADC conversion is done.*/
//
//				SPI_getNewmeasurementData();
//
//				while(spi_dataReceivedFlag == FALSE) {}														/*@brief Block until the ADC conversion data is been received via SPI.*/

//				memcpy(rawMeasureData_localBytePtr, SPI_getReceiveBuffer(), NO_OF_BYTES_ADC_AND_TIME_DIFF);	/*@brief Fill rawMeasureData_localBytePtr with the received data from SPI.*/

				uint16_t timeDifference = intervalCount;//TimersAndCo_GetTimeDifference();

				rawMeasureData_localBytePtr += (NO_OF_BYTES_ADC_AND_TIME_DIFF -2);
				*rawMeasureData_localBytePtr 		= (timeDifference >> 8);								/*@brief Add the MSB of the time difference.*/
				*(++rawMeasureData_localBytePtr) 	= (timeDifference & 0x0F);								/*@brief Add the LSB of the time difference.*/
				rawMeasureData_localBytePtr = rawMeasureData_localBytePtrAdd;								/*@brief Reset tasks_localBytePointer to its first byte.*/


				if ((1 == usb_cdcVcomPtr->attach) && (1 == usb_cdcVcomPtr->startTransactions))				/*@brief If CDC class is ready to send proceed.*/
				{
				memcpy(usb_sendBufferPtr, rawMeasureData_localBytePtr, NO_OF_BYTES_ADC_AND_TIME_DIFF);
				*usb_sendBufferSizePtr += NO_OF_BYTES_ADC_AND_TIME_DIFF ;

				error = USB_DeviceCdcAcmSend(usb_cdcVcomPtr->cdcAcmHandle, USB_CDC_VCOM_BULK_IN_ENDPOINT, usb_sendBufferPtr, NO_OF_BYTES_ADC_AND_TIME_DIFF);	/*@brief Send the USB send buffer data over USB.*/
				*usb_sendBufferSizePtr = 0;

					if (error != kStatus_USB_Success)
					{
						//printf("Failure to send Data over USB!\r\n");
					}
				}
			}

		  *timeIntervalPassedFlag = 0;
		}
	}
}

/*
 * @brief Task to set the new configuration data received from USB.
 * This task has the highest priority while the new configuration is set all measurements are postponed.
 */
static void vTaskSetNewConfigData(void *pvParameters)
{
	uint8_t* convertedConfigData_localBytePtr = (uint8_t*) pvPortMalloc(NO_OF_BYTE_CONFIG_DATA);
	memset(convertedConfigData_localBytePtr, '\0', NO_OF_BYTE_CONFIG_DATA);
	uint8_t* convertedConfigData_localBytePtrAdd = convertedConfigData_localBytePtr;													/*@brief Get the address of the local byte pointer to easily reset tasks_localBytePointer back to its first byte.*/

	uint8_t* rawConfigData_localBytePtr = (uint8_t*) pvPortMalloc(NO_OF_BYTE_IN_ROW);
	memset(rawConfigData_localBytePtr, '\0', NO_OF_BYTE_IN_ROW);
	uint8_t* rawConfigData_localBytePtrAdd = rawConfigData_localBytePtr;


	for(;;)
	{
		if(xQueueReceive(xConfigDataQueue, rawConfigData_localBytePtr, portMAX_DELAY) == pdPASS)										/*@brief Block until new configuration data is been received.*/
		{
			printf("Config data from queue received.\n");

			int i;
            for (i = 0; i < NO_OF_BYTE_IN_ROW; i++)
            {
                printf("%d ",*rawConfigData_localBytePtr - 48);
                rawConfigData_localBytePtr++;
            }
            rawConfigData_localBytePtr = rawConfigData_localBytePtrAdd;
            printf("\n");

			memset(convertedConfigData_localBytePtr, '\0', NO_OF_BYTE_CONFIG_DATA);
			convertedConfigData_localBytePtr = Conversions_convertConfigurationDataToByte(rawConfigData_localBytePtr); 					/*@brief Convert the configuration data from ASCII to byte format.*/

            for (i = 0; i < NO_OF_BYTE_CONFIG_DATA; i++)
            {
                printf("%d ",*convertedConfigData_localBytePtr);
                convertedConfigData_localBytePtr++;
            }
            convertedConfigData_localBytePtr = convertedConfigData_localBytePtrAdd;
            printf("\n");

			/*@brief Configuration of the ADC configuration pins.*/
			GPIO_WritePinOutput(ADC_RESET_PORT, ADC_RANGE_PIN, HIGH);																	/*@brief Reset ADC, keep the reset min 25ns high, the following lines take longer.*/

			GPIO_WritePinOutput(ADC_RANGE_PORT, ADC_RANGE_PIN, *convertedConfigData_localBytePtr);										/*@brief Set configuration value to pin ADC_RANGE.*/
			GPIO_WritePinOutput(ADC_OS0_PORT, ADC_OS0_PIN, *(convertedConfigData_localBytePtr += 2));									/*@brief Set configuration value to pin ADC_OS0.*/
			GPIO_WritePinOutput(ADC_OS1_PORT, ADC_OS1_PIN, *(convertedConfigData_localBytePtr += 2));									/*@brief Set configuration value to pin ADC_OS1.*/
			GPIO_WritePinOutput(ADC_OS2_PORT, ADC_OS2_PIN, *(convertedConfigData_localBytePtr += 2));									/*@brief Set configuration value to pin ADC_OS2.*/
			/*
			 * Uncomment the following if I2C communication is connected.*************************************
			 * Be aware that the program get stuck here if the I2C communication was unsuccessful!
			 */
//			while(I2C_MasterTransfer((convertedConfigData_localBytePtr += 2), GAIN0_I2C_SLAVE_ADD, kI2C_Write) != kStatus_Success)		/*@brief Wait until sending the configuration value for gain0 is confirmed.*/
//			{
//				vTaskToggle_LED(1000);		/*@brief While communication is unsuccessful flash the red led every second.*/
//			}
//			while(I2C_MasterTransfer((convertedConfigData_localBytePtr += 2), GAIN1_I2C_SLAVE_ADD, kI2C_Write) != kStatus_Success)		/*@brief Wait until sending the configuration value for gain1 is confirmed.*/
//			{
//				vTaskToggle_LED(1000);		/*@brief While communication is unsuccessful flash the red led every second.*/
//			}
//			while(I2C_MasterTransfer((convertedConfigData_localBytePtr += 2), GAIN2_I2C_SLAVE_ADD, kI2C_Write) != kStatus_Success)		/*@brief Wait until sending the configuration value for gain2 is confirmed.*/
//			{
//				vTaskToggle_LED(1000);		/*@brief While communication is unsuccessful flash the red led every second.*/
//			}
//			while(I2C_MasterTransfer((convertedConfigData_localBytePtr += 2), GAIN3_I2C_SLAVE_ADD, kI2C_Write) != kStatus_Success)		/*@brief Wait until sending the configuration value for gain3 is confirmed.*/
//			{
//				vTaskToggle_LED(1000);		/*@brief While communication is unsuccessful flash the red led every second.*/
//			}
//			vTaskClear_LED();

			convertedConfigData_localBytePtr = convertedConfigData_localBytePtrAdd;

			GPIO_WritePinOutput(ADC_RESET_PORT, ADC_RANGE_PIN, LOW);																	/*@brief Reset ADC, set the reset pin back to low.*/

			firstConfigDoneFlag = TRUE;
		}
	}
}
/*
 * @brief Task to initialise the system at the start.
 */
static void vTaskInitSystem(void *pvParameters)
{
	/*@brief Initialisation of the GPIO PINS and its interrupts.*/
    gpio_pin_config_t inputConfig = PIN_AS_INPUT;
    gpio_pin_config_t outputConfig = PIN_AS_OUTPUT;

    GPIO_PinInit(ADC_OS0_PORT, 				ADC_OS0_PIN, 				&outputConfig);
    GPIO_PinInit(ADC_OS1_PORT, 				ADC_OS1_PIN, 				&outputConfig);
    GPIO_PinInit(ADC_OS2_PORT, 				ADC_OS2_PIN, 				&outputConfig);
    GPIO_PinInit(ADC_RANGE_PORT, 			ADC_RANGE_PIN, 				&outputConfig);
    GPIO_PinInit(ADC_CONVST_PORT, 			ADC_CONVST_PIN, 			&outputConfig);
    GPIO_PinInit(ADC_BUSY_PORT, 			ADC_BUSY_PIN, 				&inputConfig);
    GPIO_PinInit(ADC_FRST_DATA_PORT, 		ADC_FRST_DATA_PIN, 			&inputConfig);
    GPIO_PinInit(ADC_RESET_PORT, 			ADC_RESET_PIN,				&outputConfig);
    GPIO_PinInit(FILTER_CLOCK_PORT, 		FILTER_CLOCK_PIN,			&outputConfig);
    GPIO_PinInit(BUTTON_START_MEASURE_PORT, BUTTON_START_MEASURE_PIN,	&inputConfig);
    GPIO_PinInit(PCB_LED_PORT, 				PCB_LED_PIN,				&outputConfig);
    GPIO_PinInit(RGB_LED_RED_PORT, 			RGB_LED_RED_PIN,			&outputConfig);

    PORT_SetPinInterruptConfig(ADC_BUSY_INTERRUPT_PORT, ADC_BUSY_PIN, ADC_BUSY_INTERRUPT_Type);
    NVIC_SetPriority(ADC_BUSY_INTERRUPT_IRQn, ADC_BUSY_INTERRUPT_IRQn);
    EnableIRQ(ADC_BUSY_INTERRUPT_IRQn);
    PORT_SetPinInterruptConfig(BUTTON_START_MEA_INTERRUPT_PORT, BUTTON_START_MEASURE_PIN, BUTTON_START_MEA_INTERRUPT_Type);
    NVIC_SetPriority(BUTTON_START_MEA_INTERRUPT_IRQn, BUTTON_START_MEA_INTERRUPT_IRQn);
    EnableIRQ(BUTTON_START_MEA_INTERRUPT_IRQn);

	tasks_interruptFlags 	= FALSE;
	startButtonPushedFlag	= FALSE;
	ADC_busyFlag			= FALSE;
	firstConfigDoneFlag		= FALSE;

	//volatile uint8_t* delay_MS_PassedFlagPtr = TimersAndCo_GetDelay_MS_PassedFlagPtr();

	/*@brief Initialising of the drivers*/
	TimersAndCo_initialisation();
	SPI_initialisation();
	I2C_initialisation(100000, I2C1_IRQn, I2C1, I2C1_CLK_SRC, 0x01);
	//SD_card_initialisation();
	USB_DeviceApplicationInit();

	/*@brief Initialising task needed for the configuration.*/
	xTaskCreate(vTaskSetNewConfigData, "TaskSetNewConfigData", 800, NULL, configMAX_PRIORITIES -1, NULL);

	/* @brief Queue initialisation.*/
	xConfigDataQueue						= xQueueCreate(1, NO_OF_BYTE_CONFIG_DATA);									/*@brief The configuration data queue.*/

	/*@brief Initialising vTaskGetMeasurement, needed to start measurements.*/
	xTaskCreate(vTaskGetMeasurement, "TaskGetMeasurement", 800, NULL, 3, NULL);

	/*@brief Deleting the initialisation task and xButtonSTART_MEASURE_pressedSemaphore.*/
	//vPortFree(localBytePointer);
	vTaskDelete(NULL);
}
/*
 * @brief Task to toggle PCB_LED & RGB_LED_RED.
 * @param uint64_t intervalMilliseconds
 */
static void vTaskToggle_LED(uint64_t intervalMilliseconds)
{
	volatile uint8_t* MS_delayFlagPtr	= TimersAndCo_GetDelay_MS_FlagPtr();

	GPIO_WritePinOutput(PCB_LED_PORT, PCB_LED_PIN, HIGH);
	GPIO_WritePinOutput(RGB_LED_RED_PORT,RGB_LED_RED_PIN, HIGH);
	TimersAndCo_Start_MS_delay(intervalMilliseconds);
	while(*MS_delayFlagPtr == TRUE) {;;}
	GPIO_WritePinOutput(PCB_LED_PORT, PCB_LED_PIN, LOW);
	GPIO_WritePinOutput(RGB_LED_RED_PORT,RGB_LED_RED_PIN, LOW);
	TimersAndCo_Start_MS_delay(intervalMilliseconds);
	while(*MS_delayFlagPtr == TRUE) {;;}
}
/*
 * @brief Task to set PCB_LED & RGB_LED_RED LOW.
 */
static void vTaskClear_LED(void)
{
	GPIO_WritePinOutput(PCB_LED_PORT, PCB_LED_PIN, LOW);
	GPIO_WritePinOutput(RGB_LED_RED_PORT,RGB_LED_RED_PIN, LOW);
}
/**************************************public methods*********************************************/
/*
 * @brief Initialisation for Tasks.
 */
void Tasks_initialisation(void)
{
	/*@brief Initialising task for initialisation.*/
	xTaskCreate(vTaskInitSystem, "TaskInitSystem", 2000, NULL, 4, NULL);
}

