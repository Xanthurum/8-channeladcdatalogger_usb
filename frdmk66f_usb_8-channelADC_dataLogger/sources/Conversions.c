/*
 * Conversions.c
 *
 *  Created on: 13 May 2018
 *      Author: Pascal Barbary
 */
/****************************************Standard C Included Files*************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/****************************************SDK Included Files.*******************************************/
#include "board.h"
#include "fsl_debug_console.h"
/**************************************FreeRTOS kernel includes.***************************************/

/**************************************Local c-files includes.******************************************/
#include "DataLoggerProjectDEFINES.h"
/**************************************public attributes************************************************/
#include "Conversions.h"
/**************************************private attributes*****************************************/

/**************************************private prototypes*****************************************/

/**************************************private methods********************************************/

/**************************************public methods*********************************************/
/*
 * @brief Function to construct a measurement line in a readable decimal format.
 * @param uint8_t* dataToFormatPtr -> The pointer to the data to format.
 */
uint8_t* Conversions_constructDecimalFormattedMeasureLine(uint8_t* dataToFormatPtr)
{
	static uint8_t measurementDataLine[NO_OF_BYTE_IN_ROW];							/*@brief Local array/pointer to a decimal constructed measurement line.*/
	memset(measurementDataLine, '\0', NO_OF_BYTE_IN_ROW);
	uint8_t* measurementDataLinePtr = NULL;											/*@brief Pointer to a specific byte of the decimal constructed measurement line.*/
	uint8_t placeToAddDecimal = 0;													/*@brief The location in the array where to place the decimal value in the measurement line.*/

	uint8_t i = 0;
	uint16_t tempDecimal = 0;

	for(i = 1; i <= (NO_OF_BYTE_MEASURE_DATA/2); i++)
	{
		tempDecimal = 0;
		tempDecimal += *dataToFormatPtr++;											/*@brief Merge MSB and LSB of a measurement into an uint16_t.*/
		tempDecimal = (tempDecimal << 8) + *dataToFormatPtr++;

		if(i < (NO_OF_BYTE_MEASURE_DATA/2))
		{
			measurementDataLinePtr = &measurementDataLine[placeToAddDecimal];
			sprintf(measurementDataLinePtr, "%5d,", tempDecimal);					/*@brief Convert uint16_t to decimal value, add ',' and remember the # of used bytes for the conversion.*/
			placeToAddDecimal += 6;
		}
		else
		{
			measurementDataLinePtr = &measurementDataLine[placeToAddDecimal];
			sprintf(measurementDataLinePtr, "%5d\r\n", tempDecimal);					/*@brief End of measurement line so add '\r' & '\n' and remember the # of used bytes for the conversion.*/
		}
	}
	return measurementDataLine;
}
/*
 * @brief Function to convert the string formatted configuration data read to single byte values.
 * @param uint8_t* dataToFormatPtr -> The pointer to the data to convert.
 */
uint8_t* Conversions_convertConfigurationDataToByte(uint8_t* dataToConvertPtr)
{
	uint8_t i = 0;
	uint8_t j = 0;
	uint8_t tempValue = 0;
	static uint8_t configurationData[NO_OF_BYTE_CONFIG_DATA];						/*@brief Local array/pointer to configuration data.*/

	while(i < MAX_NO_OF_BYTE_CONFIG_DATA_FILE)
	{
		if(dataToConvertPtr[i +1] == ',' || dataToConvertPtr[i +1] == '\0')			/*@brief If statement to test if the configuration value has 1,2 or 3 digits.*/
		{
			configurationData[j] = Conversions_ASCIItoINT(dataToConvertPtr[i]);
			j++;
			i += 2;
		}
		else 	if(dataToConvertPtr[i +2] == ',' || dataToConvertPtr[i +2] == '\0')
		{
			tempValue = 0;
			tempValue += Conversions_ASCIItoINT(dataToConvertPtr[i]) *10;
			tempValue += Conversions_ASCIItoINT(dataToConvertPtr[i +1]);
			configurationData[j] = tempValue;
			j++;
			i += 3;
		}
		else 	if(dataToConvertPtr[i +3] == ',' || dataToConvertPtr[i +3] == '\0')
		{
			tempValue = 0;
			tempValue += Conversions_ASCIItoINT(dataToConvertPtr[i]) *100;
			tempValue += Conversions_ASCIItoINT(dataToConvertPtr[i +1]) *10;
			tempValue += Conversions_ASCIItoINT(dataToConvertPtr[i +2]);
			configurationData[j] = tempValue;
			j++;
			i += 4;
		}
	}

	return configurationData;
}
/*
 * @brief Function to convert ASCII numbers to uint8_t
 */
uint8_t Conversions_ASCIItoINT(uint8_t ASCIInumberToConvert)
{
	return (uint8_t)((int)ASCIInumberToConvert - 48);
}


