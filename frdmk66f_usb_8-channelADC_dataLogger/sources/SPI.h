/*
 * SPI.h
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

#ifndef SPI_H_
#define SPI_H_
/****************************************Standard C Included Files*************************************/
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/****************************************SDK Included Files.*******************************************/
#include "fsl_common.h"
/**************************************public attributes************************************************/



/**************************************public prototypes************************************************/
/*
 * @brief Initialisation of DSPI.
 */
void SPI_initialisation(void);
/*
 * @brief Getter for the receive buffer.
 * @return uint8_t* dspi_receiveBuffer	-> The DSPI receive buffer.
 */
uint8_t* SPI_getReceiveBuffer();
/*
 * @brief DSPI receive request for new measurement data.
 */
void SPI_getNewmeasurementData();
/*
 * @brief Getter forspi_dataReceivedFlag pointer.
 */
volatile uint8_t* SPI_get_dataReceivedFlag();

#endif /* SPI_H_ */
