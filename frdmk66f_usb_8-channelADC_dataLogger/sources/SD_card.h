/*
 * SD_card.h
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

#ifndef SD_CARD_H_
#define SD_CARD_H_
/****************************************Standard C Included Files*************************************/
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/****************************************SDK Included Files.*******************************************/
#include "fsl_common.h"
/**************************************public attributes************************************************/



/**************************************public prototypes************************************************/
/*
 * @brief Initialisation for a sd card.
 */
void SD_card_initialisation(void);
/*
 * @brief Function to write to the sd card.
 */
void SD_card_write(void);
/*
 * @brief Function to get the configuration data pointer from the sd card.
 * @return uint8_t* sd_configData	-> Pointer to the bytes configuration data.
 */
uint8_t* SD_card_getConfigDataPtr(void);
/*
 * @brief Function to get the measurement data pointer from the sd card.
 * @return uint8_t* sd_measurementDataPtr	-> Pointer to the bytes of the last measurement data.
 */
uint8_t* SD_card_getMeasurementDataPtr(void);

#endif /* SD_CARD_H_ */
