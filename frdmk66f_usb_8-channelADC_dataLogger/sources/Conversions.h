/*
 * Conversions.h
 *
 *  Created on: 13 May 2018
 *      Author: xanth
 */

#ifndef CONVERSIONS_H_
#define CONVERSIONS_H_

/**************************************public prototypes************************************************/
/*
 * @brief Function to construct a measurement line in a readable decimal format.
 * @param uint8_t* dataToFormatPtr -> The pointer to the data to format.
 */
uint8_t* Conversions_constructDecimalFormattedMeasureLine(uint8_t* dataToFormatPtr);
/*
 * @brief Function to convert the string formatted configuration data read to single byte values.
 * @param uint8_t* dataToFormat -> The data to convert.
 */
uint8_t* Conversions_convertConfigurationDataToByte(uint8_t* dataToConvertPtr);
/*
 * @brief Function to convert ASCII numbers to uint8_t
 */
uint8_t Conversions_ASCIItoINT(uint8_t ASCIInumberToConvert);

#endif /* CONVERSIONS_H_ */
