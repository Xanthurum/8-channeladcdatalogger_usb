/*
 * Tasks.h
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

#ifndef TASKS__H_
#define TASKS__H_
/****************************************Standard C Included Files*************************************/
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
/****************************************SDK Included Files.*******************************************/
#include "fsl_common.h"
/**************************************FreeRTOS kernel includes.***************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

/**************************************public attributes************************************************/

/**************************************public prototypes************************************************/
/*
 * @brief Initialisation for Tasks.
 */
void Tasks_initialisation(void);


#endif /* TASKS__H_ */
