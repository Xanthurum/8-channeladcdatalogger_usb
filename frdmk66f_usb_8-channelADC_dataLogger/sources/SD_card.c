/*
 * SD_card.c
 *
 *  Created on: 10 Apr 2018
 *      Author: Pascal Barbary
 */

/****************************************SDK Included Files.*******************************************/
#include "fsl_sd.h"
#include "fsl_debug_console.h"
#include "ff.h"
#include "fsl_common.h"
#include "diskio.h"
#include "fsl_sd_disk.h"
#include "board.h"
#include "fsl_sysmpu.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK66F18.h"
/**************************************FreeRTOS kernel includes.***************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"
/**************************************Local h-files includes.******************************************/
#include "DataLoggerProjectDEFINES.h"
#include "Conversions.h"
#include "SD_card.h"

/**************************************private attributes*****************************************/

static FATFS 		sd_fileSystem; 						/*@brief File system object.*/
static FIL 			sd_fileObject;						/*@brief File object.*/
static DIR 			sd_directory;						/*@brief Directory object structure.*/
static UINT 		sd_readBytes;						/*@brief The number of read bytes.*/
static UINT 		sd_writtenBytes;					/*@brief The number of written bytes.*/
static uint32_t 	sd_bufferedBytes;					/*@brief The number of buffered bytes in the write buffer.*/
static uint32_t 	sd_writtenBuffersInFile;			/*@brief The number of buffers written in the current file.*/
static uint32_t 	sd_writtenFiles;					/*@brief The number of written files.*/
static char* 		sd_filePathPtr;						/*@brief Pointer to the current file path*/
static uint8_t* 	sd_measurementDataPtr;				/*@brief Pointer to the last received measurement data.*/
static uint8_t* 	sd_measureDataPtrAdd;				/*@brief Pointer to the first byte of the measurement data pointer so sd_measurementDataPtr can be easily reset to its first byte.*/
static uint8_t 		sd_status;							/*@brief State return value from function.*/
static uint32_t 	sd_i;								/*@brief Variable free to use for the functions.*/
static char*		sd_measurementDataLinePtr;			/*@brief Pointer to a decimal constructed measurement line.*/
static char*		sd_measurementDataLinePtrAdd;		/*@brief Pointer to the first byte of the decimal constructed measurement line pointer.*/
//static uint8_t 		sd_noOfMeasurementDataLineBytes;	/*@brief The number bytes after conversion with sprinf a constructed measurement line has.*/
static uint8_t*		sd_configurationDataPtr;			/*@brief Pointer to the conversed configuration data*/
static uint8_t*		sd_configurationDataPtrAdd;			/*@brief Pointer to the first byte of the configuration data pointer so sd_configurationDataPtr can be easily reset to its first byte.*/
/*
 * Definitions of physical drive number for each drive
 * RAMDISK         0        ram disk to physical drive 0
 * USBDISK         1        usb disk to physical drive 1
 * SDDISK          2        sd disk to physical drive 2
 * MMCDISK         3        mmc disk to physical drive 3
 * SDSPIDISK       4        sdspi disk to physical drive 4
 */
static const TCHAR sd_driverNumberBuffer[3U] = {SDDISK + '0', ':', '/'};

/* @brief decription about the read/write buffer
* The size of the read/write buffer should be a multiple of 512, since SDHC/SDXC card uses 512-byte fixed
* block length and this driver example is enabled with a SDHC/SDXC card.If you are using a SDSC card, you
* can define the block length by yourself if the card supports partial access.
* The address of the read/write buffer should align to the specific DMA data buffer address align value if
* DMA transfer is used, otherwise the buffer address is not important.
* At the same time buffer address/size should be aligned to the cache line size if cache is supported.
*/
SDK_ALIGN(char sd_bufferWrite[SDK_SIZEALIGN(BUFFER_SIZE, SDMMC_DATA_BUFFER_ALIGN_CACHE)], MAX(SDMMC_DATA_BUFFER_ALIGN_CACHE, SDMMCHOST_DMA_BUFFER_ADDR_ALIGN));
SDK_ALIGN(uint8_t sd_bufferRead[SDK_SIZEALIGN(BUFFER_SIZE, SDMMC_DATA_BUFFER_ALIGN_CACHE)],  MAX(SDMMC_DATA_BUFFER_ALIGN_CACHE, SDMMCHOST_DMA_BUFFER_ADDR_ALIGN));

/*! @brief SDMMC host detect card configuration */
static const sdmmchost_detect_card_t s_sdCardDetect = {
														#ifndef BOARD_SD_DETECT_TYPE
															.cdType = kSDMMCHOST_DetectCardByGpioCD,
														#else
															.cdType = BOARD_SD_DETECT_TYPE,
														#endif
															.cdTimeOut_ms = (~0U),
														};

/**************************************private prototypes*****************************************/
/*
 * @brief Function to detect a sd card insertion.
 */
static status_t SD_card_waitCardInsert(void);
/*
 * @brief Function to open a directory.
 * @param DIR* directoryPtr			-> Pointer to directory object.
 * @param const TCHAR* dirPathPtr	-> Pointer to the directory path, ex. "/Config".
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_openDirectory(DIR* directoryPtr, TCHAR* dirPathPtr);
/*
 * @brief Function to open a file, to read or create a file.
 * @param FIL* filePtr				-> Pointer to blank file object.
 * @param const TCHAR* filePathPtr	-> Pointer to the file path, ex. "/Config/config.txt".
 * @param BYTE mode					-> Access mode and file open mode flags, ex. FILE_READ or FILE_CREATE.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_openFile(FIL* filePtr, TCHAR* filePathPtr, BYTE mode);
/*
 * @brief Function to read a file.
 * @param FIL* filePtr				-> Pointer to the file object to read.
 * @param void* readBufferPtr		-> Pointer to the read buffer in which the read bytes will be returned.
 * @param UINT bytesToRead			-> The number of bytes to read.
 * @param UINT* bytesReadPtr		-> Pointer to the number of bytes that have been read.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_readFile(FIL* filePtr, void* readBufferPtr, UINT bytesToRead, UINT* bytesReadPtr);
/*
 * @brief Function to write in a file.
 * @param FIL* filePtr				-> Pointer to the file object to write in.
 * @param void* writeBufferPtr		-> Pointer to the write buffer with the bytes to write.
 * @param UINT bytesToWrite			-> The number of bytes to write.
 * @param UINT* bytesWrittenPtr		-> Pointer to the number of bytes that have been written.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_writeInFile(FIL* filePtr, void* writeBufferPtr, UINT bytesToWrite, UINT* bytesWrittenPtr);
/*
 * @brief Function to close a file.
 * @param FIL* filePtr				-> Pointer to the file object to be closed.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_closeFile(FIL* filePtr);
/*
 * @brief Function to construct a file path name.
 */
static void SD_card_constructFilePath(void);

/**************************************private methods********************************************/
/*
 * @brief Function to detect a sd card insertion.
 */
static status_t SD_card_waitCardInsert(void)
{
    /* Save host information. */
    g_sd.host.base = SD_HOST_BASEADDR;
    g_sd.host.sourceClock_Hz = SD_HOST_CLK_FREQ;
    /* card detect type */
    g_sd.usrParam.cd = &s_sdCardDetect;
    /* SD host initialisation function */
    if (SD_HostInit(&g_sd) != kStatus_Success)
    {
        PRINTF("\r\nSD host init fail\r\n");
        return kStatus_Fail;
    }
    /* power off card */
    SD_PowerOffCard(g_sd.host.base, g_sd.usrParam.pwr);
    /* wait card insert */
    if (SD_WaitCardDetectStatus(SD_HOST_BASEADDR, &s_sdCardDetect, true) == kStatus_Success)
    {
        PRINTF("\r\nCard inserted.\r\n");
        /* power on the card */
        SD_PowerOnCard(g_sd.host.base, g_sd.usrParam.pwr);
    }
    else
    {
        PRINTF("\r\nCard detect fail.\r\n");
        return kStatus_Fail;
    }

    return kStatus_Success;
}
/*
 * @brief Function to open a directory.
 * @param DIR* directoryPtr			-> Pointer to directory object.
 * @param const TCHAR* dirPathPtr	-> Pointer to the directory path, ex. "/Config".
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_openDirectory(DIR* directoryPtr, TCHAR* dirPathPtr)
{

    if (f_opendir(directoryPtr, (const char*)dirPathPtr))
    {
        PRINTF("Open directory failed.\r\n");
        return kStatus_Fail;
    }
    else
    {
    	PRINTF("Open directory successful.\r\n");
    	return kStatus_Success;
    }
}
/*
 * @brief Function to open a file, to read or create a file.
 * @param FIL* filePtr				-> Pointer to blank file object.
 * @param const TCHAR* filePathPtr	-> Pointer to the file path, ex. "/Config/config.txt".
 * @param BYTE mode					-> Access mode and file open mode flags, ex. FA_READ.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_openFile(FIL* filePtr, TCHAR* filePathPtr, BYTE mode)
{
   if (f_open(filePtr, (const char*)filePathPtr, mode))
    {
    	PRINTF("Open file failed.\r\n");
        return kStatus_Fail;
    }
    else
    {
    	PRINTF("Open file successful.\r\n");
    	return kStatus_Success;
    }
}
/*
 * @brief Function to read a file.
 * @param FIL* filePtr				-> Pointer to the file object to read.
 * @param void* readBufferPtr		-> Pointer to the read buffer in which the read bytes will be returned.
 * @param UINT bytesToRead			-> The number of bytes to read.
 * @param UINT* bytesReadPtr		-> Pointer to the number of bytes that have been read.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_readFile(FIL* filePtr, void* readBufferPtr, UINT bytesToRead, UINT* bytesReadPtr)
{
	memset(sd_bufferRead, '\0', sizeof(sd_bufferRead));		/*@brief (Re)Initialise buffer.*/

    if (f_read(filePtr, readBufferPtr, bytesToRead, bytesReadPtr))
    {
    	PRINTF("Read  from file failed.\r\n");
        return kStatus_Fail;
    }
    else
    {
    	return kStatus_Success;
    }
}
/*
 * @brief Function to write in a file.
 * @param FIL* filePtr				-> Pointer to the file object to write in.
 * @param void* writeBufferPtr		-> Pointer to the write buffer with the bytes to write.
 * @param UINT bytesToWrite			-> The number of bytes to write.
 * @param UINT* bytesWrittenPtr		-> Pointer to the number of bytes that have been written.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_writeInFile(FIL* filePtr, void* writeBufferPtr, UINT bytesToWrite, UINT* bytesWrittenPtr)
{
    if (f_write(filePtr, writeBufferPtr, bytesToWrite, bytesWrittenPtr))
    {
    	PRINTF("Write to file failed.\r\n");
        return kStatus_Fail;
    }
    else
    {
    	//PRINTF("Write to file successful.\r\n");
    	return kStatus_Success;
    }
}
/*
 * @brief Function to close a file.
 * @param FIL* filePtr				-> Pointer to the file object to be closed.
 * @return status_t status			-> The status of the execution, success = 0 and fail = 1.
 */
static status_t SD_card_closeFile(FIL* filePtr)
{
    if (f_close(filePtr))
    {
    	PRINTF("Closing file has failed.\r\n");
        return kStatus_Fail;
    }
    else
    {
    	return kStatus_Success;
    }
}
/*
 * @brief Function to construct a file path name.
 */
static void SD_card_constructFilePath(void)
	{
		memset(sd_filePathPtr, '\0', 32);								/*@brief Initialise the file path pointer.*/
		char* filePathPtrAddress = sd_filePathPtr;						/*@brief The file path pointer its first byte address.*/

		sd_filePathPtr = strcat(sd_filePathPtr, FILE_PATH_MEASURE);		/*@brief Add the first part to the file path name, aka "/Measure/file".*/

		sd_filePathPtr += NO_OF_BYTES_FILE_PATH_MEASURE;				/*@brief Place file path pointer where the integer need to be added.*/

		sprintf(sd_filePathPtr, "%d", sd_writtenFiles);					/*@brief Add an integer, the no of written files, to the char pointer.*/

		sd_filePathPtr = filePathPtrAddress;							/*@brief Reset the file path pointer to its first byte.*/

		sd_filePathPtr = strcat(sd_filePathPtr, FILE_EXTENSION);		/*@brief Add the file extension to the file path name, aka ".text".*/

		printf("the address of the file path pointer = %d, The file path = ", sd_filePathPtr) ;
		while((*sd_filePathPtr) != '\0')
		{
			printf("%c", (char) *sd_filePathPtr++);
		}
		printf(".\n");
		sd_filePathPtr = filePathPtrAddress;
	}

/**************************************public methods*********************************************/
/*
 * @brief Initialisation for a sd card.
 */
void SD_card_initialisation(void)
{
	SYSMPU_Enable(SYSMPU, false);
	/*
	 * @brief Initialise buffers and variables.
	 */
	memset(sd_bufferRead, '\0', BUFFER_SIZE);
	memset(sd_bufferWrite, '\0', BUFFER_SIZE);
	sd_measurementDataPtr 			= (uint8_t*) malloc(NO_OF_BYTE_MEASURE_DATA);
	memset(sd_measurementDataPtr, '\0', NO_OF_BYTE_MEASURE_DATA);
	sd_measureDataPtrAdd			= sd_measurementDataPtr;				/*@brief Get the address of the measurement data pointer to easily reset sd_measurementDataPtr back to its first byte.*/
	sd_filePathPtr					= (char*) malloc(32);
	sd_measurementDataLinePtr		= (char*) malloc(NO_OF_BYTE_IN_ROW);
	memset(sd_measurementDataLinePtr, '\0', NO_OF_BYTE_IN_ROW);
	sd_measurementDataLinePtrAdd	= sd_measurementDataLinePtr;
	sd_configurationDataPtr			= (uint8_t*) malloc(NO_OF_BYTE_CONFIG_DATA);
	memset(sd_configurationDataPtr, '\0', NO_OF_BYTE_CONFIG_DATA);
	sd_configurationDataPtrAdd		= sd_configurationDataPtr;
	sd_readBytes 					= 0;
	sd_writtenBytes					= 0;
	sd_writtenBuffersInFile			= 0;
	sd_writtenFiles					= 1;
	sd_status						= 0;
	sd_i							= 0;

	/*
	 * @brief Wait for a sd card to be inserted.
	 */
	while(SD_card_waitCardInsert() != kStatus_Success)
	{

	}

    /* Mount/Unmount a Logical Drive
     * @param FATFS* fs			-> Pointer to the file system object (NULL:unmount)
     * @param const TCHAR* path -> Logical drive number to be mounted/unmounted
     * @param BYTE opt			-> Mode option 0:Do not mount (delayed mount), 1:Mount immediately
     */
    if (f_mount(&sd_fileSystem, sd_driverNumberBuffer, 0U))
    {
        PRINTF("Mount volume failed.\r\n");
    }
    /*
     * _FS_RPATH
     * This option configures support of relative path.
     * 0: Disable relative path and remove related functions.
     * 1: Enable relative path. f_chdir() and f_chdrive() are available.
     * 2: f_getcwd() function is available in addition to 1.
     */
	#if (_FS_RPATH >= 2U)
		/*
		 * f_chdrive() 				-> Change Current Directory or Current Drive, Get Current Directory
		 * @param const TCHAR* path	-> Drive number
		 */
		if (f_chdrive((char const *)&sd_driverNumberBuffer[0U]))
		{
			PRINTF("Change drive failed.\r\n");
		}
	#endif

		/*
		 * @brief Read the configuration data from the sd card and write it to sd_configDataPtr.
		 */
		SD_card_openDirectory(&sd_directory, DIR_PATH_CONFIG);																/*@brief Open the configuration directory.*/
		SD_card_openFile(&sd_fileObject, FILE_PATH_CONFIG, FILE_READ);														/*@brief Open the configuration file in read mode.*/
		SD_card_readFile(&sd_fileObject, sd_bufferRead, MAX_NO_OF_BYTE_CONFIG_DATA_FILE, &sd_readBytes);					/*@brief Read the configuration file and write the configuration data to sd_bufferRead.*/
		SD_card_closeFile(&sd_fileObject);																					/*@brief Close the configuration file, only for safety in read mode.*/

		/*
		 * @brief Create the first file to write the measurement data to.
		 */
		SD_card_openDirectory(&sd_directory, DIR_PATH_MEASURE);																/*@brief Open the measurement directory.*/
		SD_card_constructFilePath();																						/*@brief Construct a new file path, with the first file name.*/
		SD_card_openFile(&sd_fileObject, sd_filePathPtr, FILE_CREATE);														/*@brief Create the new file.*/

		memcpy(sd_configurationDataPtr, Conversions_convertConfigurationDataToByte(sd_bufferRead), NO_OF_BYTE_CONFIG_DATA);	/*@brief Fill sd_configurationDataPtr with the from ASCII to byte converted configuration data.*/
}
/*
 * @brief Function to write to the sd card.
 */
void SD_card_write(void)
{
//	printf("SD sd_measurementDataPtr = ");
//				for(sd_i = 0; sd_i < NO_OF_BYTE_MEASURE_DATA; sd_i++)
//				{
//					if(sd_i < (NO_OF_BYTE_MEASURE_DATA - 1)) printf("%d, ", *sd_measurementDataPtr++);
//					else printf("%d \r\n ", *sd_measurementDataPtr++);
//				}
//				sd_measurementDataPtr = sd_measureDataPtrAdd;

	memcpy(sd_measurementDataLinePtr, Conversions_constructDecimalFormattedMeasureLine(sd_measurementDataPtr), NO_OF_BYTE_IN_ROW); /*@brief Fill sd_measurementDataLinePtr with the new and formatted measurement data.*/
//	printf("SD sd_measurementDataLinePtr = ");
//					printf("%s\r\n  ", sd_measurementDataLinePtr);


	memcpy(&sd_bufferWrite[sd_bufferedBytes], sd_measurementDataLinePtr, NO_OF_BYTE_IN_ROW);
	sd_bufferedBytes += NO_OF_BYTE_IN_ROW;

	sd_measurementDataPtr = sd_measureDataPtrAdd;												/*@brief Relocate the measurement data pointer to the first byte.*/
	memset(sd_measurementDataPtr, '\0', NO_OF_BYTE_MEASURE_DATA);								/*@brief Reinitialise the measurement data pointer.*/

	if(sd_bufferedBytes > (BUFFER_SIZE - NO_OF_BYTE_IN_ROW))									/*@brief If # buffered bytes is greater then (BUFFER_SIZE - (a complete row - 1)), then the buffer can be written to the file on the sd card.*/
	{
		SD_card_writeInFile(&sd_fileObject, sd_bufferWrite, sd_bufferedBytes, &sd_writtenBytes);
		memset(sd_bufferWrite, '\0', sizeof(sd_bufferWrite));
//	printf("Just before reset, sd_bufferedBytes = %d\r\n", sd_bufferedBytes);
		sd_bufferedBytes = 0;																	/*@brief Reset the number of bytes written in the buffer.*/
		sd_writtenBuffersInFile++;																/*@brief Increment the number of written buffers.*/
//	printf("SD write done, sd_writtenBuffersInFile = %d\r\n", sd_writtenBuffersInFile);
	}

	if(sd_writtenBuffersInFile == MAX_BUFFERS_IN_FILE)											/*@brief If the maximum # of buffers that can be written in a file, a new file has to be created on the sd card.*/
	{
		sd_writtenBuffersInFile = 0;															/*@brief Reset the number of buffers written in the file.*/
		SD_card_closeFile(&sd_fileObject);														/*@brief Close the current file in use.*/
		sd_writtenFiles++;																		/*@brief Increment the number of written files.*/
		printf("New file made, sd_writtenFiles = %d\r\n", sd_writtenFiles);
		SD_card_constructFilePath();															/*@brief Construct a new file path, with a new file name.*/
		SD_card_openFile(&sd_fileObject, sd_filePathPtr, FILE_CREATE);							/*@brief Create the new file.*/
	}
}
/*
 * @brief Function to get the configuration data pointer from the sd card.
 * @return uint8_t* sd_configDataPtr	-> Pointer to the bytes configuration data.
 */
uint8_t* SD_card_getConfigDataPtr(void)
{
	return sd_configurationDataPtr;
}
/*
 * @brief Function to get the measurement data pointer from the sd card.
 * @return uint8_t* sd_measurementDataPtr	-> Pointer to the bytes of the last measurement data.
 */
uint8_t* SD_card_getMeasurementDataPtr(void)
{
	return sd_measurementDataPtr;
}
